package handler

import (
	"goapi/pkg/docs"

	"github.com/gofiber/fiber/v2"
	fiberSwagger "github.com/swaggo/fiber-swagger"
	"github.com/uptrace/bun"
)

func RegisterRoutes(app *fiber.App, db *bun.DB) {
	app.Get("/healthz", func(c *fiber.Ctx) error {
		return c.SendStatus(200)
	})
	// แก้ไขรายละเอียด api doc
	docs.SwaggerInfo.Title = "Todo Service API Document"
	docs.SwaggerInfo.Description = "List of APIs for Todo Service."
	docs.SwaggerInfo.Version = "1.0"

	app.Get("/swagger/*", fiberSwagger.WrapHandler)

	api := app.Group("/api")
	v1 := api.Group("/v1")
	todo := v1.Group("/todos")

	todoHandler := todoHandler{db}

	// Create new todo
	todo.Post("/", todoHandler.CreateTodo)

	// List all todo
	todo.Get("/", todoHandler.ListTodo)

	// Get todo by id
	todo.Get("/:id", todoHandler.GetTodo)

	// Update Todo status by Id
	todo.Patch("/:id", todoHandler.UpdateTodoStatus)

	// Delete Todo by Id
	todo.Delete("/:id", todoHandler.DeleteTodo)
}
