package main

import (
	"flag"
	"fmt"
	"goapi/pkg/common/config"
	"goapi/pkg/common/database"
	"goapi/pkg/common/logger"
	"goapi/pkg/handler"
	"goapi/pkg/middleware"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/fiber/v2/middleware/requestid"
)

func main() {

	var timeout time.Duration
	flag.DurationVar(&timeout, "graceful-timeout", time.Second*15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
	flag.Parse()
	logger.Info("Service is starting")

	cfg, err := config.LoadConfig()
	if err != nil {
		panic(err)
	}

	db, closeDB, err := database.Init(cfg.Dsn)
	if err != nil {
		panic(err)
	}
	defer closeDB()
	logger.Info("Database connected")

	app := fiber.New()

	app.Use(cors.New())
	app.Use(requestid.New())
	app.Use(middleware.Logger)
	app.Use(recover.New())

	handler.RegisterRoutes(app, db)

	serverShutdown := make(chan struct{})

	go gracefulShutdown(app, serverShutdown)

	err = app.Listen(fmt.Sprintf(":%v", cfg.Port))
	if err != nil && err != http.ErrServerClosed {
		panic(err.Error())
	}

	select {
	case <-serverShutdown:
		log.Println("Shutdown completed")
	case <-time.After(timeout):
		log.Println("Shutdown timeout")
	}

	// <-serverShutdown
	log.Println("Running cleanup tasks")
	// Your cleanup tasks go here
}

func gracefulShutdown(srv *fiber.App, serverShutdown chan struct{}) {
	// Listen for syscall signals for process to interrupt/quit
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	s := <-sig
	log.Printf("Received %v signal...", s)

	err := srv.Shutdown()
	if err != nil {
		log.Fatalf("Server shutdown failed: %+v\n", err)
	}
	serverShutdown <- struct{}{}
}
